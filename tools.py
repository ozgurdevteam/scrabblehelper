# -*- coding: utf-8 -*-
import os

__author__ = 'ozgur'
__creation_date__ = '23.08.2017' '21:30'

dbfolder = "db"
rawfolder = os.path.join(dbfolder, "raw")


def convert_take_first(filename, splitter):
    f = open(os.path.join(rawfolder, filename), "r")
    lines = f.readlines()
    f.close()
    outtext = ""
    for line in lines:
        outtext += line.split(splitter)[0] + "\n"
    outtext = outtext[:-1]
    f = open(os.path.join(dbfolder, filename), "w")
    f.write(outtext)
    f.close()


if __name__ == '__main__':
    convert_take_first("zemberek.list", " ")
