Small scrabble helper with Turkish words

**Usage** ./kelimelib.py [PARAMS]

```bash

search str value is 'de' or 'd' and int value is 3 in examples
    
    PARAMS:
    -h: shows this help
    -b: starts with [-b:de]
    -f: ends with [-f:de]
    -e: exact position [-p:3.d] [-p:-3.d]
    -i: includes [-i:de]
    -x: max length [-x:3]
    -n: min length [-n:3]
    -l: exact length [-e:3]    

```

Mehmet �zg�r Bayhan - 2017 - BSD License