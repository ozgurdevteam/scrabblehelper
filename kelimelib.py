#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

__author__ = 'Mehmet Ozgur Bayhan'
__creation_date__ = '23.08.2017' '21:04'


class Search:
    def __init__(self):
        kfile = open(os.path.join(os.path.dirname(os.path.realpath(__file__)),"db","kelime.list"), "r")
        self.klist = kfile.read().split("\n")
        kfile.close()
        self.errors = []

    # noinspection PyBroadException
    def _filter_partial(self, sign: str, value) -> None:
        """
        Serach function for each arg
        :param sign: search type sign
        :param value: search value
        :return: None
        """
        new_list = []

        if sign == "b":
            for text in self.klist:
                try:
                    if text.startswith(value):
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        elif sign == "f":
            for text in self.klist:
                try:
                    if text.endswith(value):
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        elif sign == "e":
            try:
                position, char = value.split(".")
                position = int(position)
            except Exception as ex:
                self.errors.append((sign, value, ex.args))
                return
            for text in self.klist:
                try:
                    if text[position] == char:
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        elif sign == "i":
            for text in self.klist:
                try:
                    if value in text[1:-1]:
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        elif sign == "x":
            try:
                value = int(value)
            except Exception as ex:
                self.errors.append((sign, value, ex.args))
                return
            for text in self.klist:
                try:
                    if len(text) <= value:
                        new_list.append(text)
                except Exception:
                    pass

        elif sign == "n":
            try:
                value = int(value)
            except Exception as ex:
                self.errors.append((sign, value, ex.args))
                return
            for text in self.klist:
                try:
                    if len(text) >= value:
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        elif sign == "l":
            try:
                value = int(value)
            except Exception as ex:
                self.errors.append((sign, value, ex.args))
                return
            for text in self.klist:
                try:
                    if len(text) == value:
                        new_list.append(text)
                except Exception as ex:
                    self.errors.append((sign, value, ex.args))

        self.klist = new_list.copy()

    def search(self, args: list) -> list:
        """
        searches the arguemnts in the word list
        :param args: search arguments 
        :return: list
        """
        parsed = False
        for arg in args:
            try:
                if arg[0] == "-":
                    arg = arg[1:]
                sign, value = arg.split(":")
                self._filter_partial(sign, value)
                parsed = True

            except Exception as ex:
                self.errors.append((args, ex.args))
        if parsed:
            return self.klist
        else:
            return []


def print_help():
    print("""
    search str value is 'de' or 'd' and int value is 3 in examples
    
    PARAMS:
    -h: shows this help
    -b: starts with [-b:de]
    -f: ends with [-f:de]
    -e: exact position [-e:3.d] [-e:-3.d]
    -i: includes [-i:de]
    -x: max length [-x:3]
    -n: min length [-n:3]
    -l: exact length [-l:3]    
    """)


if __name__ == '__main__':
    if len(sys.argv) == 1 or "-h" in sys.argv:
        print_help()
    else:
        search = Search()
        print("searching...\r", end="")
        results = search.search(sys.argv[1:])
        print("RESULTS:        ")
        for word in results:
            print(word)
        if len(search.errors) > 0:
            print("ERRORS:        ")
            for line in search.errors:
                print(line)




